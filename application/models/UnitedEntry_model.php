<?php

class UnitedEntry_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'pagination'));
        $this->load->helper('url');
        $this->load->database();
        $this->load->dbutil();
    }

    public function create($data, $foldername)
    {
        $this->db->insert('united_' . $foldername, $data);
        return TRUE;
    }

    public function allrecord($foldername)
    {
        $this->db->select('*');
        $this->db->from('united_' . $foldername);
        $rs = $this->db->get();
        return $rs->result_array();
    }

    public function allSelectedRecordLimit($foldername,$limit)
    {
        $query = $this->db->query("SELECT * FROM united_$foldername order by RAND() limit $limit");
        return $query->result_array();
    }

    public function data_list($limit, $offset, $tab1, $tab2, $tab3)
    {
        $query = $this->db->query("SELECT * FROM (SELECT * FROM united_$tab1 UNION SELECT * FROM united_$tab2 UNION SELECT * FROM united_$tab3) as result WHERE result.featured_list = 1 limit $offset,$limit");
        return $query->result_array();
    }

    public function edit_data($table, $id)
    {
        $query = $this->db->query("SELECT * FROM united_$table WHERE id = $id");
        $data = $query->result_array();
        return $data[0];
    }

    public function CollectionSet($tab1, $tab2, $tab3)
    {
        $query = $this->db->query("SELECT * FROM (SELECT * FROM united_$tab1 UNION SELECT * FROM united_$tab2 UNION SELECT * FROM united_$tab3) as result WHERE result.featured_list = 1");
        return $query->num_rows();
    }

    public function about_list($limit, $offset, $tab1, $tab2, $tab3)
    {
        $query = $this->db->query("SELECT * FROM united_$tab1 UNION SELECT * FROM united_$tab2 UNION SELECT * FROM united_$tab3 limit $offset,$limit");
        return $query->result_array();
    }
}