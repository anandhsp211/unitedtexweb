<div class="page-heading about-heading header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content">
<!--                    <h4>about us</h4>-->
                    <h2>About us</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="best-features about-features">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>About Us</h2>
                </div>
            </div>
            <div class="col-md-6">
                <div class="right-image">
                    <img width="100%" height="100%" src="<?php echo base_url(); ?>skin/assets/images/owner.jpg" alt="TamilSelvan">
                </div>
            </div>
            <div class="col-md-6">
                <div class="left-content">
                    <h4>Who we are &amp; What we do?</h4>
                    <p> United tex is one of the world top most trading company.
                        We are handling all types of textile machine, Sapares and Fabrics. <br><br>
                        And also we can take bulk order for towels and shirts.
                        We have a separate rapier machine to weave the designs as per the user~order requirements. <br><br>
                        We have a full energetic team to handling the shipping process of the rapier machines. <br><br>
                        We Trading  All Type Of Textile Related Products Like Weaving, warping, knitting, sewing, spinning, doubling, embroidery, yarns And Fabrics. <br><br>

                        It Is One Of The Platform For Buyers An Sellers In Textile Field, buyers Can Choose The Best Product With Good Quality And Also Seller Can Sale The High Quantity Of Products
                    </p>
                    <ul class="social-icons">
                        <li><a target="_blank" href="https://www.facebook.com/united2017/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/tamil-selvan-13533220/"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-members">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Team Members</h2>
                </div>
            </div>

            <?php
            if (isset($happy)) {
                if (isset($happy) && is_array($happy) && count($happy)) {
                    foreach ($happy as $datas) { ?>
                        <div class="col-md-4">
                            <div class="team-member">
                                <div class="thumb-container">
                                    <a data-fancybox="images"
                                       href="<?php echo base_url(); ?>uploads/happy/<?php echo $datas['image_name'] ?>">
                                        <img class="img-fluid lazy"
                                             width="100%" height="100%"
                                             data-src="<?php echo base_url(); ?>uploads/happy/<?php echo $datas['image_name'] ?>"
                                             src="<?php echo base_url(); ?>uploads/happy/<?php echo $datas['image_name'] ?>"
                                             alt="">
                                    </a>
                                    <div class="hover-effect">
                                        <div class="hover-content">
                                            <ul class="social-icons">
                                                <li><a target="_blank" href="https://www.facebook.com/united2017/"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="https://www.linkedin.com/in/tamil-selvan-13533220/"><i class="fa fa-linkedin"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="down-content">
                                    <h4><?php echo trim($datas['name']) ?></h4>
                                    <span><?php echo trim($datas['position']) ?></span>
                                    <p><?php echo trim($datas['descr']) ?></p>
                                </div>
                            </div>
                        </div>
                    <?php }
                }
            }
            ?>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="service-item">
                    <div class="icon">
                        <i class="fa fa-empire"></i>
                    </div>
                    <div class="down-content">
                        <h4>Looms</h4>
                        <p>The purpose of any loom is to hold the warp threads under tension to facilitate the interweaving of the weft threads.</p>
                        <a href="<?php echo base_url(); ?>products" class="filled-button">See Products</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-item">
                    <div class="icon">
                        <i class="fa fa-gear"></i>
                    </div>
                    <div class="down-content">
                        <h4>Spares</h4>
                        <p>It's an interchangeable part that is kept in an inventory and used for the repair or replacement of failed units.</p>
                        <a href="<?php echo base_url(); ?>products" class="filled-button">See Products</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="service-item">
                    <div class="icon">
                        <i class="fa fa-scissors"></i>
                    </div>
                    <div class="down-content">
                        <h4>Fabrics</h4>
                        <p>The common of which are for clothing like shirts, towels and for containers such as bags and baskets.</p>
                        <a href="<?php echo base_url(); ?>products" class="filled-button">See Products</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Products</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel">
                    <?php
                    if (isset($data)) {
                        if (isset($data) && is_array($data) && count($data)) {
                            foreach ($data as $datas) { ?>
                                <div class="client-item recent_works_item">
                                    <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                        <img class="img-fluid"
                                             width="100%" height="100%"
                                             src="<?php echo base_url(); ?>uploads/<?php echo trim($datas['value']); ?>/<?php echo trim($datas['image_name']); ?>"
                                             alt=""/>
                                    </a>
                                    <div class="down-content">
                                        <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                            <h4><?php echo trim($datas['name']); ?></h4>
                                        </a>
                                        <h6>&#8377;<?php echo trim($datas['price']); ?></h6>
                                        <p><?php echo trim($datas['descr']); ?></p>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-md-8">
                            <h4>Planning to opt for a service like this?</h4>
                            <p>Call us now.</p>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo base_url(); ?>contact" class="filled-button">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>