<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <p>Copyright &copy; <?php echo date ( "Y" ) ?>. All Rights Reserved. unitedtex.in - By:
                        <a rel="nofollow noopener" href="<?php echo base_url(); ?>">Anandh</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php if ($this->session->flashdata('message')) { ?>
    <div class="toast">
        <div class="alert alert-info">
            <strong><?php echo $this->session->flashdata('message') ?></strong>
        </div>
    </div>
<?php } ?>
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url(); ?>skin/assets/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Additional Scripts -->
<script src="<?php echo base_url(); ?>skin/assets/js/owl.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/slick.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/isotope.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/accordions.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/jquery.fancybox.min.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/jquery.lazy.min.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>skin/assets/js/custom.js"></script>

<script language = "text/Javascript">
    cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
    function clearField(t){                   //declaring the array outside of the
        if(! cleared[t.id]){                      // function makes it static and global
            cleared[t.id] = 1;  // you could use true and false, but that's more typing
            t.value='';         // with more chance of typos
            t.style.color='#fff';
        }
    }
</script>
</body>
</html>
</body>
</html>