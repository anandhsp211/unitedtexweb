<!-- Page Content -->
<div class="page-heading products-heading header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content">
                    <!--<h4>new arrivals</h4>-->
                    <h2>Our Products</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="filters">
                    <ul>
                        <!--<li class="active" data-filter="*">All Services</li>-->
<!--                        <li data-filter=".spares">Spares</li>-->
                        <li class="spares_active" data-filter=".looms">Machinery</li>
                        <li data-filter=".fabric">Fabric</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-12">
                <div class="filters-content">
                    <div class="row grid">
                        <?php
                        if (isset($data)) {
                            if (isset($data) && is_array($data) && count($data)) {
                                foreach ($data as $datas) { ?>
                                    <div class="col-lg-4 col-md-4 all <?php echo $datas['value'] ?>">
                                        <div class="product-item">
                                            <a data-fancybox="images"
                                               href="<?php echo base_url(); ?>uploads/<?php echo trim($datas['value']); ?>/<?php echo trim($datas['image_name']); ?>">
                                                <img class="img-fluid lazy"
                                                     width="100%" height="100%"
                                                     data-src="<?php echo base_url(); ?>uploads/<?php echo trim($datas['value']); ?>/<?php echo trim($datas['image_name']); ?>"
                                                     src="<?php echo base_url(); ?>/uploads/<?php echo trim($datas['value']); ?>/<?php echo trim($datas['image_name']); ?>"
                                                     alt="">
                                            </a>
                                            <div class="down-content">
                                                <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                                    <h4><?php echo trim($datas['name']); ?></h4>
                                                </a>
                                                <h6>&#8377;<?php echo trim($datas['price']); ?></h6>
                                                <p><?php echo trim($datas['descr']); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-md-8">
                            <h4>Planning to opt for a service like this?</h4>
                            <p>Call us now.</p>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo base_url(); ?>contact" class="filled-button">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Products</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel">
                    <?php
                    if (isset($data)) {
                        if (isset($data) && is_array($data) && count($data)) {
                            foreach ($data as $datas) { ?>
                                <div class="client-item recent_works_item">
                                    <a
                                       href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                        <img class="img-fluid"
                                             width="100%" height="100%"
                                             src="<?php echo base_url(); ?>uploads/<?php echo trim($datas['value']); ?>/<?php echo trim($datas['image_name']); ?>"
                                             alt=""/>
                                    </a>
                                    <div class="down-content ">
                                        <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                            <h4><?php echo trim($datas['name']); ?></h4>
                                        </a>
                                            <h6>&#8377;<?php echo trim($datas['price']); ?></h6>
                                            <p><?php echo trim($datas['descr']); ?></p>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>