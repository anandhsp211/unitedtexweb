<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">
    <h2 class="text-center wow animated pulse">Please login into continue</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
        <form method="post" action="<?php echo site_url('home/login_data'); ?>" class="col-lg-4 form_bg col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="text" required class="form-control" id="username" name="username"
                               placeholder="username">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <input type="password" required class="form-control" id="password" name="password"
                               placeholder="password">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Login</button>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login_info text-center">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow animated fadeInDown">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                    <p>Just click on the "LOG IN" button to continue with login information. </p>
                </div>
            </div>
        </form>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>