<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">
    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Add Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Our Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Add Products</a>
    </div>
    <h2 class="text-center wow animated pulse">Edit Product</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <a href="<?php echo base_url(); ?>home/productlist" class="filled-button">Back</a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
        <form method="post" enctype="multipart/form-data" action="<?php echo site_url('home/producteditUpload'); ?>"
              class="col-lg-8 add_record form_bg col-md-12 col-sm-12 col-xs-12 text-center">

            <input type="hidden" name="id" value="<?php if (isset($data) && $data) {
                echo trim($data['id']);
            } ?>"/>
            <input type="hidden" value="<?php if (isset($data) && $data) {
                echo trim($data['image_name']);
            } ?>" class="form-control" required name="image_old_name">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="image_path">Select Type:</label>
                        <select name="image_path" required class="form-control" id="image_path">
                            <option value="">--Select--</option>
                            <option <?php if (isset($data) && $data['value'] == 'fabric') {
                                echo 'selected';
                            } ?> value="fabric">Fabric
                            </option>
                            <option <?php if (isset($data) && $data['value'] == 'looms') {
                                echo 'selected';
                            } ?> value="looms">Looms
                            </option>
                            <option <?php if (isset($data) && $data['value'] == 'spares') {
                                echo 'selected';
                            } ?> value="spares">Spares
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="name">Enter Name:</label>
                        <input type="text" value="<?php if (isset($data) && $data) {
                            echo trim($data['name']);
                        } ?>" required class="form-control" id="name" name="name" placeholder="Enter Name">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="price">Enter Price:</label>
                        <input type="number" value="<?php if (isset($data) && $data) {
                            echo trim($data['price']);
                        } ?>" class="form-control" id="price" name="price" placeholder="Enter Price">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="qty">Enter Quantity:</label>
                        <input type="number" value="<?php if (isset($data) && $data) {
                            echo trim($data['qty']);
                        } ?>" class="form-control" id="qty" name="qty" required placeholder="Enter Quantity">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desc">Enter Description:</label>
                        <textarea name="descr" required class="form-control" rows="5"
                                  id="desc"><?php if (isset($data) && $data) {
                                echo trim($data['descr']);
                            } ?></textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desc">Featured List:</label>
                        <div class="form-group options">
                            <input type="checkbox" <?php if (isset($data) && $data['featured_list'] == 1) {
                                echo 'checked';
                            } ?> name="featured_list" value="1" /> Yes
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label for="doc">Select Document</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <input id="doc" type="file" name="userfile" size="20"/>
                        <img src="<?php if($data['image_name'] && file_exists('uploads/'.$data['value'].'/'.trim($data['image_name']))){
                            echo base_url().'uploads/'.$data['value'].'/'.trim($data['image_name']);
                        }else{
                            echo base_url().'skin/assets/images/glow.gif';
                        } ?>"
                             class="img-thumbnail img-responsive  happy_image_fr"
                             title="<?php echo trim($data['name']) ?>" alt="<?php echo trim($data['name']) ?>">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Submit</button>
            </div>

        </form>
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>