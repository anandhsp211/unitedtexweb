<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">
    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Add Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Our Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Add Products</a>
    </div>
    <div class="container">
        <h2 class="text-center wow animated pulse">Product List</h2>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <a href="<?php echo base_url(); ?>home/productadd" class="filled-button">Add Product List</a>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link <?php if(isset($high) && $high=='looms'){echo 'active';} ?> " data-toggle="tab" href="#looms">Looms</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if(isset($high) && $high=='spares'){echo 'active';} ?>" data-toggle="tab" href="#spares">Spares</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php if(isset($high) && $high=='fabric'){echo 'active';} ?>" data-toggle="tab" href="#fabric">Fabric</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="looms" class="container tab-pane <?php if(isset($high) && $high=='looms'){echo 'active';} ?>"><br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($looms)){
                                    if (isset($looms) && is_array($looms) && count($looms)) {
                                        foreach ($looms as $loom) {  ?>
                                            <tr class="success">
                                                <td> <?php echo trim($loom['id']); ?></td>
                                                <td><?php echo trim($loom['name']); ?></td>
                                                <td>
                                                    <img src="<?php if($loom['image_name'] && file_exists('uploads/looms/'.trim($loom['image_name']))){
                                                        echo base_url().'uploads/looms/'.trim($loom['image_name']);
                                                    }else{
                                                        echo base_url().'skin/assets/images/glow.gif';
                                                    } ?>" class="img-thumbnail img-responsive happy_image_fr" title="<?php echo trim($loom['name']); ?>" alt="<?php echo trim($loom['name']); ?>">
                                                </td>
                                                <td><?php echo trim($loom['price']); ?></td>
                                                <td><?php echo trim($loom['qty']); ?></td>
                                                <td><?php echo trim($loom['value']); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>home/productedit/<?php echo trim($loom['id']); ?>?looms=true" class="filled-button">Edit</a>
                                                    <a href="<?php echo base_url(); ?>home/productdelete/<?php echo trim($loom['id']); ?>?looms=true" class="filled-button">Delete</a>
                                                </td>
                                            </tr>
                                        <?php   }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="spares" class="container tab-pane <?php if(isset($high) && $high=='spares'){echo 'active';} ?>"><br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($spares)){
                                    if (isset($spares) && is_array($spares) && count($spares)) {
                                        foreach ($spares as $spare) {  ?>
                                            <tr class="success">
                                                <td> <?php echo trim($spare['id']); ?></td>
                                                <td><?php echo trim($spare['name']); ?></td>
                                                <td>
                                                    <img src="
                                                    <?php if($spare['image_name'] && file_exists('uploads/spares/'.trim($spare['image_name']))){
                                                        echo base_url().'uploads/spares/'.trim($spare['image_name']);
                                                    }else{
                                                        echo base_url().'skin/assets/images/glow.gif';
                                                    } ?>" class="img-thumbnail img-responsive happy_image_fr" title="<?php echo trim($spare['name']) ?>" alt="<?php echo trim($spare['name']); ?>">
                                                </td>
                                                <td><?php echo trim($spare['price']); ?></td>
                                                <td><?php echo trim($spare['qty']); ?></td>
                                                <td><?php echo trim($spare['value']); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>home/productedit/<?php echo trim($spare['id']); ?>?spares=true" class="filled-button">Edit</a>
                                                    <a href="<?php echo base_url(); ?>home/productdelete/<?php echo trim($spare['id']); ?>?spares=true" class="filled-button">Delete</a>
                                                </td>
                                            </tr>

                                        <?php   }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="fabric" class="container tab-pane <?php if(isset($high) && $high=='fabric'){echo 'active';} ?>"><br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                 if(isset($fabric)){
                                    if (isset($fabric) && is_array($fabric) && count($fabric)) {
                                        foreach ($fabric as $fabrics) {  ?>
                                            <tr class="success">
                                                <td> <?php echo trim($fabrics['id']); ?></td>
                                                <td><?php echo trim($fabrics['name']); ?></td>
                                                <td>
                                                    <img src="<?php if($fabrics['image_name'] && file_exists('uploads/fabric/'.trim($fabrics['image_name']))){
                                                        echo base_url().'uploads/fabric/'.trim($fabrics['image_name']);
                                                    }else{
                                                        echo base_url().'skin/assets/images/glow.gif';
                                                    } ?>" class="img-thumbnail img-responsive happy_image_fr" title="<?php echo trim($fabrics['name']); ?>" alt="<?php echo trim($fabrics['name']) ?>">
                                                </td>
                                                <td><?php echo trim($fabrics['price']); ?></td>
                                                <td><?php echo trim($fabrics['qty']); ?></td>
                                                <td><?php echo trim($fabrics['value']); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>home/productedit/<?php echo trim($fabrics['id']); ?>?fabric=true" class="filled-button">Edit</a>
                                                    <a href="<?php echo base_url(); ?>home/productdelete/<?php echo trim($fabrics['id']); ?>?fabric=true" class="filled-button">Delete</a>
                                                </td>
                                            </tr>

                                        <?php   }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>