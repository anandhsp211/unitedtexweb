<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">

    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Add Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Our Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Add Products</a>
    </div>
    <h2 class="text-center wow animated pulse">Happy Customers</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <a href="<?php echo base_url(); ?>home/happylist" class="filled-button">Back</a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
         <form method="post" enctype="multipart/form-data" action="<?php echo site_url('home/happyeditupload' ); ?>" class="col-lg-8 add_record form_bg col-md-12 col-sm-12 col-xs-12 text-center">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="name">Enter Name:</label>
                        <input type="text" value="<?php if(isset($data) && $data){echo trim($data['name']);}?>" required class="form-control" id="name" name="name" placeholder="Enter Name">
                        <input type="hidden" value="<?php if(isset($data) && $data){echo trim($data['image_name']);}?>" class="form-control"  name="image_old_name">
                        <input type="hidden" name="id" value="<?php if(isset($data) && $data){echo trim($data['id']);}?>"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="position">Enter Position:</label>
                        <input type="text" value="<?php if(isset($data) && $data){echo trim($data['position']);}?>" class="form-control" required id="position" name="position" placeholder="Enter Price">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desc">Enter Description:</label>
                        <textarea name="descr"  class="form-control" rows="5" id="desc"><?php if(isset($data) && $data){echo trim($data['descr']);}?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label for="doc">Select Photo</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <img src="<?php echo base_url(); ?>uploads/happy/<?php echo trim($data['image_name']) ?>" class="img-thumbnail img-responsive happy_image_fr" title="<?php echo trim($data['name']) ?>" alt="<?php echo trim($data['name']) ?>">
                        <input required id="doc" type="file" name="userfile" size="20"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Submit</button>
            </div>

        </form>
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>