<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">
    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Add Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Our Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Add Products</a>
    </div>
    <h2 class="text-center wow animated pulse">Add New Products</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding_top_ten">
        <a href="<?php echo base_url(); ?>home/productlist" class="filled-button">Back</a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
        <form method="post" enctype="multipart/form-data" action="<?php echo site_url('home/productUpload'); ?>" class="col-lg-8 add_record form_bg col-md-12 col-sm-12 col-xs-12 text-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="image_path">Select Type:</label>
                        <select name="image_path" required class="form-control" id="image_path">
                            <option value="">--Select--</option>
                            <option value="fabric">Fabric</option>
                            <option value="looms">Looms</option>
                            <option value="spares">Spares</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="name">Enter Name:</label>
                        <input type="text" required class="form-control" id="name" name="name" placeholder="Enter Name">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="price">Enter Price:</label>
                        <input type="number" class="form-control" id="price" name="price" placeholder="Enter Price">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="qty">Enter Quantity:</label>
                        <input type="number" required class="form-control" id="qty" name="qty" placeholder="Enter Quantity">
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desc">Enter Description:</label>
                        <textarea name="descr" required class="form-control" rows="5" id="comment"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="desc">Featured List:</label>
                        <div class="form-group options">
                            <input type="checkbox" name="featured_list" value="1" /> Yes
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label for="doc">Select Document</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <input id="doc" type="file" name="userfile" size="20"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Submit</button>
            </div>

        </form>
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>