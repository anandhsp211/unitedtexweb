<div class="page-heading contact-heading header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content">
                    <h4>Sell</h4>
                    <h2>let’s sell the products with us</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="send-message">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Send us a Message</h2>
                </div>
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <form enctype="multipart/form-data" action="<?php echo site_url('united/sell_mail'); ?>" name="sell" id="contact" method="post">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 padding_bottom_twenfive">
                                <fieldset>
                                    <select name="category" required class="form-control" id="image_path">
                                        <option value="">--Category--</option>
                                        <option value="Fabric">Fabric</option>
                                        <option value="Looms">Looms</option>
                                        <option value="Spares">Spares</option>
                                    </select>
                                </fieldset>
                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="country" type="text" class="form-control" id="country"
                                           placeholder="Country Name" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="name" type="text" class="form-control" id="name"
                                           placeholder="Full Name" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="email" type="text" class="form-control" id="email"
                                           placeholder="E-Mail Address" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="phoneNumber" type="tel" class="form-control" id="mobile"
                                           placeholder="Mobile" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="subject" type="text" class="form-control" id="subject"
                                           placeholder="Subject" required>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message"
                                              placeholder="Your Message" required></textarea>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                        <input id="doc" required multiple type="file" name="userFiles[]" size="20"/>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <button type="submit" id="myFile" class="filled-button">Send Message</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Recent Works</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel ">
                    <?php
                    if(isset($recent)){
                        if (isset($recent) && is_array($recent) && count($recent)) {
                            foreach ($recent as $datas) {  ?>
                                <div class="client-item recent_works_item">
                                    <a data-fancybox="images" href="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>">
                                        <img class="img-fluid lazy" data-src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" alt="">
                                    </a>
                                    <div class="down-content">
                                        <a href="#"><h4><?php echo trim($datas['name']); ?></h4></a>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
