<div class="banner header-text">
    <div class="owl-banner owl-carousel">
        <div class="banner-item-01">
            <div class="text-content">
                <h4>New Arrivals</h4>
                <h2> On Looms</h2>
            </div>
        </div>
        <div class="banner-item-03">
            <div class="text-content">
                <h4>Best Offer</h4>
                <h2>On Spares</h2>
            </div>
        </div>
        <div class="banner-item-02">
            <div class="text-content">
                <h4>New Arrivals</h4>
                <h2>On Fabrics</h2>
            </div>
        </div>
    </div>
</div>
<div class="latest-products">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Latest Products</h2>
                    <a href="<?php echo base_url(); ?>products">view all products <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <?php
            if (isset($data)) {
                if (isset($data) && is_array($data) && count($data)) {
                    foreach ($data as $datas) { ?>
                        <div class="col-md-4">
                            <div class="product-item">
                                <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                    <img class="img-fluid"
                                         width="100%" height="100%"
                                         src="<?php if(file_exists('uploads/'.trim($datas['value']).'/'.trim($datas['image_name']))){
                                             echo base_url().'uploads/'.trim($datas['value']).'/'.trim($datas['image_name']);
                                         }else{
                                             echo base_url().'skin/assets/images/glow.gif';
                                         } ?>"
                                         alt=""/>
                                </a>
                                <div class="down-content">
                                    <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                        <h4>
                                            <?php echo trim($datas['name']); ?>
                                        </h4>
                                    </a>
                                        <h6>&#8377;<?php echo trim($datas['price']); ?></h6>
                                        <p><?php echo trim($datas['descr']); ?></p>

                                </div>
                            </div>
                        </div>
                    <?php }
                }
            }
            ?>
            <div id="Scroll" class="pagination-dive col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php if (isset($nav)) {
                    echo $nav;
                } ?>
            </div>
        </div>
    </div>
</div>
<div class="best-features">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>About United Tex</h2>
                </div>
            </div>
            <div class="col-md-6">
                <div class="left-content">
                    <h4>Looking for the best Rapier Machines?</h4>
                    <p>
                        United tex is one of the world top most trading company. We are handling all types of textile
                        machine, Spares and Fabrics.
                        <a rel="nofollow" href="<?php echo base_url(); ?>contact">
                            Contact us
                        </a> for more info.
                    </p>
                    <ul class="featured-list">
                        <li><a href="#">Rigid and flexible</a></li>
                        <li><a href="#">It weaves more rapidly than most shuttle machines</a></li>
                        <li><a href="#">It operates at speeds ranging from about 200 to 260 ppm</a></li>
                        <li><a href="#">Rapier machines - is their flexibility, which permits the laying of picks of
                                different colours</a></li>
                        <li><a href="#">Weave yarns of any type of fibre and can weave fabrics up to 110 inches in width
                                without modification</a></li>
                    </ul>
                    <a href="<?php echo base_url(); ?>about" class="filled-button">Read More</a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="right-image">
                    <img width="100%" height="100%" src="<?php echo base_url(); ?>skin/assets/images/feature-image.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-md-8">
                            <h4>Planning to opt for a service like this?</h4>
                            <p>Call us now.</p>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo base_url(); ?>contact" class="filled-button">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Recent Works</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel ">
                    <?php
                    if(isset($recent)){
                        if (isset($recent) && is_array($recent) && count($recent)) {
                            foreach ($recent as $datas) {  ?>
                                <div class="client-item recent_works_item">
                                    <a data-fancybox="images" href="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>">
                                        <img class="img-fluid lazy" data-src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" alt="">
                                    </a>
                                    <div class="down-content">
                                        <a href="#"><h4><?php echo trim($datas['name']); ?></h4></a>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>