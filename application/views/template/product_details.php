<!-- Page Content -->
<div class="page-heading products-heading header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content">
                    <h2>
                        <?php if (isset($data) && $data) {echo trim($data['name']);} ?>
                     </h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="products">
    <div class="container">
        <div class="row">
                <?php
                if (isset($data)) {
                    if (isset($data) && $data) { ?>
                        <div class="col-md-6 prod_det_left padding_top_ten">
                            <a class="col-md-12" data-fancybox="images"
                               href="<?php echo base_url(); ?>uploads/<?php echo $data['value']; ?>/<?php echo $data['image_name']; ?>">
                                <img class="img-fluid lazy"
                                     width="100%" height="100%"
                                     data-src="<?php echo base_url(); ?>uploads/<?php echo $data['value']; ?>/<?php echo $data['image_name']; ?>"
                                     src="<?php echo base_url(); ?>uploads/<?php echo $data['value']; ?>/<?php echo $data['image_name']; ?>"
                                     alt="">
                            </a>
                        </div>
                        <div class="col-md-6 padding_top_ten">
                            <div class="col-md-12 down-content padding_top_ten">
                                <a href="#"><h4><?php echo trim($data['name']); ?></h4></a>
                                <h6>&#8377;<?php echo trim($data['price']); ?></h6>
                                <p class="padding_top_ten"><?php echo trim($data['descr']); ?></p>
                            </div>
                            <div class="col-md-12 padding_top_ten">
                                <a href="<?php echo base_url(); ?>contact" class="filled-button">Buy Now</a>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
        </div>
    </div>
</div>
<div class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-content">
                    <div class="row">
                        <div class="col-md-8">
                            <h4>Planning to opt for a service like this?</h4>
                            <p>Call us now.</p>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo base_url(); ?>contact" class="filled-button">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Products</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel">
                    <?php
                    if (isset($data_combine)) {
                        if (isset($data_combine) && is_array($data_combine) && count($data_combine)) {
                            foreach ($data_combine as $datas) { ?>
                                <div class="client-item recent_works_item">
                                    <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                        <img class="img-fluid"
                                             width="100%" height="100%"
                                             src="<?php echo base_url(); ?>uploads/<?php echo $datas['value'] ?>/<?php echo $datas['image_name'] ?>"
                                             alt=""/>
                                    </a>
                                    <div class="down-content">
                                        <a href="<?php echo base_url(); ?>united/prod/<?php echo trim($datas['id']); ?>?q=<?php echo trim($datas['value']); ?>">
                                            <h4><?php echo trim($datas['name']) ?></h4>
                                        </a>
                                            <h6>&#8377;<?php echo trim($datas['price']) ?></h6>
                                            <p><?php echo trim($datas['descr']) ?></p>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>