<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">

    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Products List</a>
    </div>
    <h2 class="text-center wow animated pulse">Recent Works</h2>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center padding_top_ten">
        <a href="<?php echo base_url(); ?>home/recentlist" class="filled-button">Back</a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
        <form method="post" enctype="multipart/form-data" action="<?php echo site_url('home/recentUpload'); ?>" class="col-lg-8 add_record form_bg col-md-12 col-sm-12 col-xs-12 text-center">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="name">Place Name:</label>
                        <input type="text" required class="form-control" id="name" name="name" placeholder="Place Name">
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom">
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label for="doc">Select Document</label>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                        <input required id="doc" type="file" name="userfile" size="20"/>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="submit" class="btn btn-primary text-center wow shake animated">Submit</button>
            </div>

        </form>
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
    </div>
</div>