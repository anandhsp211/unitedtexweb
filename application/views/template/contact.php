<div class="page-heading contact-heading header-text">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-content">
                    <h4>contact us</h4>
                    <h2>let’s get in touch</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="find-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Location on Maps</h2>
                </div>
            </div>
            <div class="col-md-8">
                <div id="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3691.973792611704!2d78.07553776951973!3d11.370456608825444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1585485303804!5m2!1sen!2sin"
                            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="col-md-4">
                <div class="left-content">
                    <h4>Head Office</h4>
                    <p>We are importing machines and spares,
                        chain rapier, somet, piconol and airjet.<br>
                        <br>
                        Tamizhselvan,
                        <br>1/583, Devangapuram,<br> Periyamanali, Namakkal,<br> TamilNadu, India. <br>
                        <i class="fa fa-mobile-phone"></i>&nbsp;+919524244158
                    </p>

                    <h4>Branch office</h4>
                    <p>
                        Suthan S,<br> No 60, canal road wattala,<br> 11300, <br> Srilanka. <br>
                        <i class="fa fa-mobile-phone"></i>&nbsp;+94778503185
                    </p>
                    <ul class="social-icons">
                        <li><a target="_blank" href="https://www.facebook.com/united2017/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/tamil-selvan-13533220/"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="send-message">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Send us a Message</h2>
                </div>
            </div>
            <div class="col-md-8">
                <div class="contact-form">
                    <form action="<?php echo site_url('united/send_mail'); ?>" name="registration" id="contact"
                          method="post">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="name" type="text" class="form-control" id="name"
                                           placeholder="Full Name" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="email" type="text" class="form-control" id="email"
                                           placeholder="E-Mail Address" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="phoneNumber" type="tel" class="form-control" id="mobile"
                                           placeholder="Mobile" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <fieldset>
                                    <input name="subject" type="text" class="form-control" id="subject"
                                           placeholder="Subject" required="">
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <textarea name="message" rows="6" class="form-control" id="message"
                                              placeholder="Your Message" required=""></textarea>
                                </fieldset>
                            </div>
                            <div class="col-lg-12">
                                <fieldset>
                                    <button type="submit" id="submit" class="filled-button">Send Message</button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="accordion">
                    <li>
                        <a>Looms</a>
                        <div class="content">
                            <p>The purpose of any loom is to hold the warp threads under tension to facilitate the interweaving of the weft threads.</p>
                        </div>
                    </li>
                    <li>
                        <a>Spares</a>
                        <div class="content">
                            <p>It's an interchangeable part that is kept in an inventory and used for the repair or replacement of failed units.</p>
                        </div>
                    </li>
                    <li>
                        <a>Fabric</a>
                        <div class="content">
                            <p>The common of which are for clothing and for containers such as bags and baskets.</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="happy-clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h2>Our Recent Works</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="owl-clients owl-carousel ">
                    <?php
                    if(isset($recent)){
                        if (isset($recent) && is_array($recent) && count($recent)) {
                            foreach ($recent as $datas) {  ?>
                                <div class="client-item recent_works_item">
                                    <a data-fancybox="images" href="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>">
                                        <img class="img-fluid lazy" data-src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" src="<?php echo base_url(); ?>uploads/recent/<?php echo trim($datas['image_name']); ?>" alt="">
                                    </a>
                                    <div class="down-content">
                                        <a href="#"><h4><?php echo trim($datas['name']); ?></h4></a>
                                    </div>
                                </div>
                            <?php }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>