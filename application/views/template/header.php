<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="We Trading All Type Of Textile Related Products Like Weaving, warping, knitting, sewing, spinning, doubling, embroidery, yarns And Fabrics">
    <meta name="keywords" content="Rapier Machine Imports in Namakkal | Trading Weaving machines in Namakkal | Trading warping machines in Namakkal | Trading sewing machines in Namakkal | Trading spinning machines in Namakkal | Trading doubling machines in Namakkal | Trading embroidery machines in Namakkal | Trading yarns products in Namakkal | Trading Fabrics products in Namakkal">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
    <title>United Tex - Rapier Machine Imports in Namakkal | Trading Weaving machines in Namakkal | Trading warping machines in Namakkal | Trading sewing machines in Namakkal | Trading spinning machines in Namakkal | Trading doubling machines in Namakkal | Trading embroidery machines in Namakkal | Trading yarns products in Namakkal | Trading Fabrics products in Namakkal</title>
    <link href="<?php echo base_url(); ?>skin/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/assets/css/owl.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>skin/assets/css/jquery.fancybox.min.css">
    <link rel="icon" href="<?php echo base_url(); ?>skin/assets/images/logo.png" type="image/png" sizes="16x16">
    <script>var ScrollDiv</script>
</head>
<body>
<!-- ***** Preloader Start ***** -->
<div id="preloader">
    <div class="jumper">
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- ***** Preloader End ***** -->
<!-- Header -->
<header id="navbar" class="">
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a class="navbar-brand" href="<?php echo base_url(); ?>united">
                <img class="main_logo" src="<?php echo base_url(); ?>skin/assets/images/logo.png" alt="logo"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url(); ?>united">Home
                            <span style="display: none" class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>products">Our Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>contact">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>products">Buy</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url(); ?>sell">Sell</a>
                    </li>
                    <?php
                    $usr = $this->session->userdata('id');
                    if (isset($usr) && trim($usr != '')) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo site_url('users/logout'); ?>"> logout</a>
                        </li>
                    <?php  }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</header>