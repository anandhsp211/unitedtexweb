<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 home_page add_form">

    <div id="navbar">
        <a class="active" href="<?php echo base_url(); ?>home/happylist">Happy Customers</a>
        <a class="" href="<?php echo base_url(); ?>home/recentlist">Recent Works</a>
        <a class="active" href="<?php echo base_url(); ?>home/productlist">Products List</a>
    </div>
    <div class="container">
        <h2 class="text-center wow animated pulse">Happy Customers</h2>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <a href="<?php echo base_url(); ?>home/happy" class="filled-button">Add Happy Customers</a>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_ten">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr class="info">
                            <th>Id</th>
                            <th>Name</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>Position</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        if(isset($data)){
                        if (isset($data) && is_array($data) && count($data)) {
                            foreach ($data as $datas) {  ?>
                                <tr class="success">
                                    <td> <?php echo trim($datas['id']) ?></td>
                                    <td><?php echo trim($datas['name']) ?></td>
                                    <td>
                                        <img src="<?php echo base_url(); ?>uploads/happy/<?php echo trim($datas['image_name']) ?>" class="img-thumbnail img-responsive happy_image_fr" title="<?php echo trim($datas['name']) ?>" alt="<?php echo trim($datas['name']) ?>">
                                    </td>
                                    <td><?php echo trim($datas['descr']) ?></td>
                                    <td><?php echo trim($datas['position']) ?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>home/happyedit/<?php echo trim($datas['id']) ?>" class="filled-button">Edit</a>
                                        <a href="<?php echo base_url(); ?>home/happydelete/<?php echo trim($datas['id']) ?>" class="filled-button">Delete</a>
                                    </td>
                                </tr>

                            <?php   }
                        }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>