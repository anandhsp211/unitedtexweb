<div class="col-lg-12 col-md-12 col-sm-12 padding_top_bottom col-xs-12">
    <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('message') ?>
        </div>
    <?php } ?>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 padding_top_bottom col-xs-12">
    <div id="container" class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center add_new_record">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                <a href="<?php echo site_url('users/admin_add_data'); ?>">Click to add new Record</a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <?php $value = $this->session->userdata('username'); ?>
                <a style="padding-left: 10px;" class="pull-right" href="<?php echo site_url('users/logout'); ?>">
                    logout</a>
                <a class="pull-right" href="">Hello <?php if (isset($value)) {
                        echo $value;
                    } ?> </a>
            </div>
        </div>
        <?php
        if (isset($view_data) && is_array($view_data) && count($view_data)): $i = 1;
            foreach ($view_data as $key => $data) {
                ?>
                <div class="col-lg-7 col-md-7 col-sm-7 padding_top_bottom col-xs-7">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 div_bg">
                        <div class="col-lg-12 col-md-12 col-xs-12 action_bg col-sm-12">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 user_name">
                                <h3>
                                    <?php echo @$data['name']; ?>
                                </h3>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-6 pull-right col-sm-12 user_name">
                                <a href="<?php echo site_url('users/admin_users_delete_data/' . @$data['id'] . ''); ?>">Delete</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 description">
                            <p><?php echo @$data['description']; ?></p>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 picture">
                            <img style="width:54%;height:300px;"
                                 src="<?php echo site_url('uploads/' . $data['document_name']); ?>" alt="image"
                                 title="image_title">
                        </div>

                    </div>
                    <?php $class = '_' . $data['id'] . ' ';
                    if ($data['status'] == 'Y') {
                        $status = 'likes_totalY';
                    } else {
                        $status = '';
                    }
                    ?>
                    <div
                        class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_null padding_right_null padding_top_bottom margin_top_bottom comments_section">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                            <button type="button" id="<?php echo @$data['id']; ?>"
                                    class="btn btn-default btn-sm <?php echo $status; ?> like_button like_button<?php echo $class; ?>">
                                <span class="glyphicon glyphicon-thumbs-up"></span>
                                <span class="likes_total" style="padding-left: 5px;"><?php echo @$data['likes']; ?>
                                    &nbsp;Likes</span>
                            </button>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 text-center col-xs-12">
                            <button type="button" id="<?php echo @$data['id']; ?>"
                                    class="btn btn-default btn-sm comment_button comment_button<?php echo $class; ?>">
                                <span class="glyphicon glyphicon-comment"></span>
                                <span class="comments_total" style="padding-left: 5px;"><?php echo @$data['count']; ?>
                                    &nbsp;Comments</span>
                            </button>
                        </div>

                        <div
                            class="col-lg-12 col-md-12 col-sm-12 col-xs-12 comments_bg comments_bg<?php echo $class; ?> margin_top_bottom padding_top_bottom">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 padding_left_null">
                                    <img class="round_image" style="width:60px;height:60px;"
                                         src="<?php echo site_url('uploads/' . @$data['document_name']); ?>" alt="image"
                                         title="image_title">
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                                    <textarea class="comment comment<?php echo $class; ?>" name="comment" rows="2"
                                              cols="38"></textarea>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <button type="button" id="<?php echo @$data['id']; ?>"
                                            class="btn btn-default post_submit post_submit<?php echo $class; ?>">Post
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 inner inner<?php echo $class; ?> col-sm-12 col-xs-12">
                            </div>
                        </div>
                    </div>

                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $(".comment_button<?php echo $class; ?>").on("click", function () {
                            $(".comments_bg.comments_bg<?php echo $class; ?>").toggle();
                            var id = $(this).attr('id');
                            if (id) {
                                var postData = {
                                    'id': id
                                };
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo site_url('users/users_all_comment_data'); ?>",
                                    data: postData,
                                    success: function (data) {
                                        $(".inner.inner<?php echo $class; ?>").html('').append(data);
                                    }
                                });
                            } else {
                                alert('Some value missing');
                            }
                        });

                        $(".like_button<?php echo $class; ?>").on("click", function () {
                            var id = $(this).attr('id');
                            if (id) {
                                var postData = {
                                    'id': id
                                };
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo site_url('users/users_like_data'); ?>",
                                    data: postData,
                                    success: function (data) {
                                        var result = $.parseJSON(data);
                                        $('.like_button.like_button<?php echo $class; ?> .likes_total').text(result.likes_count + ' Likes');
                                        if (result.status == 'Y') {
                                            $(".like_button.like_button<?php echo $class; ?>").addClass("likes_totalY");
                                        } else {
                                            $(".like_button.like_button<?php echo $class; ?>").removeClass("likes_totalY");
                                        }
                                    }
                                });
                            } else {
                                alert('Some value missing');
                            }
                        });

                        $(".post_submit.post_submit<?php echo $class; ?>").on("click", function () {
                            var id = $(this).attr('id');
                            var comment = $('.comment.comment<?php echo $class; ?>').val();

                            if (id && comment) {
                                var postData = {
                                    'id': id,
                                    'comment': comment
                                };
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo site_url('users/admin_comment_data'); ?>",
                                    data: postData,
                                    success: function (data) {
                                        var result = $.parseJSON(data);
                                        if (result) {
                                            $(".comment.comment<?php echo $class; ?>").val('');
                                            $(".inner.inner<?php echo $class; ?>").append(result.html);
                                        }
                                        $('.comment_button.comment_button<?php echo $class; ?> .comments_total').text(result.count + ' comments');

                                    }
                                });
                            } else {
                                alert('Some value missing');
                            }
                        });
                    });

                </script>
            <?php
            }
        else:
            ?>
            <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_top_bottom text-center">No Records Found..</p>
        <?php
        endif;
        ?>

        <div class="col-lg-12 padding_top_bottom margin_top_bottom col-md-12 text-center col-sm-12 col-xs-12">
            <a href="<?php echo site_url('users/index'); ?>" class="btn btn-info" role="button">Back</a>
        </div>

    </div>
</div>



