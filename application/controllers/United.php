<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class United extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('UnitedEntry_model', 'united');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        if ($this->input->post('order_name') != "") {
            $value = trim($this->input->post('name'));
        } else {
            $value = str_replace("%20", ' ', ($this->uri->segment(3)) ? $this->uri->segment(3) : 0);
            if ($this->uri->segment(3) === '0') {
                echo '<script type="text/JavaScript">  
                    ScrollDiv = "#Scroll";
                 </script>';
            } else {
                echo '<script type="text/JavaScript">  
                    ScrollDiv = "";
                 </script>';
            }
        }
        $allrecord = $this->united->CollectionSet('spares', 'fabric', 'looms');
        $baseurl = base_url() . $this->router->class . '/' . $this->router->method . "/" . $value;
        $paging = array();
        $paging['base_url'] = $baseurl;
        $paging['total_rows'] = $allrecord;
        $paging['query_string_segment'] = 'per_page';
        $paging['per_page'] = 6;
        $paging['uri_segment'] = 4;
        $paging['num_links'] = 5;
        $paging['first_link'] = 'First';
        $paging['first_tag_open'] = '<li>';
        $paging['first_tag_close'] = '</li>';
        $paging['num_tag_open'] = '<li>';
        $paging['num_tag_close'] = '</li>';
        $paging['prev_link'] = 'Prev';
        $paging['prev_tag_open'] = '<li>';
        $paging['prev_tag_close'] = '</li>';
        $paging['next_link'] = 'Next';
        $paging['next_tag_open'] = '<li>';
        $paging['next_tag_close'] = '</li>';
        $paging['last_link'] = 'Last';
        $paging['last_tag_open'] = '<li>';
        $paging['last_tag_close'] = '</li>';
        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
        $paging['cur_tag_close'] = '</a></li>';
        $this->pagination->initialize($paging);
        $data['limit'] = $paging['per_page'];
        $data['number_page'] = $paging['per_page'];
        $data['offset'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : '0';
        $data['nav'] = $this->pagination->create_links();
        $data['data'] = $this->united->data_list($data['limit'], $data['offset'], 'spares', 'fabric', 'looms');
        $data['recent'] = $this->united->allSelectedRecordLimit('recent', 10);
        $this->load->view('template/header');
        $this->load->view('template/home', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function products()
    {
        $this->load->view('template/header');
        $data['spares'] = $this->united->allrecord('spares');
        $data['fabric'] = $this->united->allrecord('fabric');
        $data['looms'] = $this->united->allrecord('looms');
        $data['data'] = array_merge($data['spares'], $data['fabric'], $data['looms']);
        $this->load->view('template/products', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function prod($id)
    {
        $get_val = $this->input->get();
        if (isset($get_val) && $get_val) {
            $folder_db_sel = '';
            if (isset($get_val['q']) && $get_val['q'] == 'looms') {
                $folder_db_sel = 'looms';
            } else if (isset($get_val['q']) && $get_val['q'] == 'spares') {
                $folder_db_sel = 'spares';
            } else if (isset($get_val['q']) && $get_val['q'] == 'fabric') {
                $folder_db_sel = 'fabric';
            }
            $this->load->view('template/header');
            $data['spares'] = $this->united->allrecord('spares');
            $data['fabric'] = $this->united->allrecord('fabric');
            $data['looms'] = $this->united->allrecord('looms');
            $data['data_combine'] = array_merge($data['spares'], $data['fabric'], $data['looms']);

            if (isset($folder_db_sel) && $folder_db_sel) {
                $data['data'] = $this->united->edit_data($folder_db_sel, $id);
                $this->load->view('template/product_details', $data, FALSE);
            } else {
                $this->session->set_flashdata('message', 'There is some problem in handling the request ..');
                redirect('united');
            }
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'There is some problem in handling the request ..');
            redirect('united');
        }

    }

    public function about()
    {
        $data['data'] = $this->united->about_list(20, 0, 'spares', 'fabric', 'looms');
        $this->load->view('template/header');
        $data['happy'] = $this->united->allrecord('happy');
        $this->load->view('template/about', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function contact()
    {
        $this->load->view('template/header');
        $data['recent'] = $this->united->allSelectedRecordLimit('recent', 10);
        $this->load->view('template/contact', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function sell()
    {
        $this->load->view('template/header');
        $data['recent'] = $this->united->allSelectedRecordLimit('recent', 10);
        $this->load->view('template/sell', $data, FALSE);
        $this->load->view('template/footer');
    }

    public function send_mail()
    {
        $this->load->library('email');
        $name = $this->input->post('name');
        $phoneNumber = $this->input->post('phoneNumber');
        $email = $this->input->post('email');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $user_email = 'unitedttradinginfo@gmail.com';
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (isset($name) && isset($phoneNumber) && isset($email) && isset($subject) && isset($message)) {
                $data = array();
                $config = Array(
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'priority' => '1'
                );
                $this->email->initialize($config);
                $this->email->from($email, $name);
                $data = array(
                    'name' => $name,
                    'phoneNumber' => $phoneNumber,
                    'email' => $email,
                    'subject' => $subject,
                    'message' => $message,
                );
                $this->data['data'] = $data;
                $this->email->to($user_email); // replace it with receiver email id
                $this->email->subject($subject); // replace it with email subject
                $message = $this->load->view('email/email', $data, TRUE);
                $this->email->message($message);
                $check = $this->email->send();
                if ($check) {
                    $this->session->set_flashdata('message', 'Mail Send Successfully');
                    redirect('united/contact');
                } else {
                    $this->session->set_flashdata('message', 'There is some problem in sending the email');
                    redirect('united/contact');
                }
            } else {
                $this->session->set_flashdata('message', 'There is some problem in sending the email');
                redirect('united/contact');
            }
        } else {
            $this->session->set_flashdata('message', 'Invalid email Address');
            redirect('united/contact');
        }
    }

    public function sell_mail()
    {
        $this->load->library('email');
        $category = $this->input->post('category');
        $country = $this->input->post('country');
        $name = $this->input->post('name');
        $phoneNumber = $this->input->post('phoneNumber');
        $email = $this->input->post('email');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');
        $user_email = 'unitedttradinginfo@gmail.com';

        $filesCount = count($_FILES['userFiles']['name']);

        if ($filesCount > 2) {
            $this->session->set_flashdata('message', 'Maximum 2 images allowed at a time');
            redirect('united/sell');
        } else {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                if (isset($name) && isset($phoneNumber) && isset($email) && isset($subject) && isset($message)) {
                    $data = array();
                    $config = Array(
                        'mailtype' => 'html',
                        'priority' => '1',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => TRUE
                    );
                    $this->email->initialize($config);
                    $this->email->from($email, $name);
                    $data = array(
                        'category' => $category,
                        'country' => $country,
                        'name' => $name,
                        'phoneNumber' => $phoneNumber,
                        'email' => $email,
                        'subject' => $subject,
                        'message' => $message,
                    );
                    $this->data['data'] = $data;
                    $this->email->to($user_email); // replace it with receiver email id
                    $this->email->subject($subject); // replace it with email subject
                    $message = $this->load->view('email/sell_email', $data, TRUE);
                    $this->email->message($message);

                    $filesCount = count($_FILES['userFiles']['name']);
                    if ($_FILES['userFiles']['name'] && $filesCount) {
                        for ($i = 0; $i < $filesCount; $i++) {
                            $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                            $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                            $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                            $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                            $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                            $uploadPath = './uploads/sell';
                            $configs['upload_path'] = $uploadPath;
                            $configs['allowed_types'] = 'gif|jpg|png|txt';
                            $this->load->library('upload', $configs);
                            $this->upload->initialize($configs);
                            if (!$this->upload->do_upload('userFile')) {
                                $this->session->set_flashdata('message', 'There is some problem in sending the email');
                                redirect('united/sell');
                            } else {
                                $fileData = $this->upload->data();
                                $pathToUploadedFile = $fileData['full_path'];
                                $this->email->attach($pathToUploadedFile);
                            }
                        }
                        $check = $this->email->send();
                    } else {
                        $this->session->set_flashdata('message', 'There is some problem in sending the email');
                        redirect('united/sell');
                    }

                    if ($check) {
                        $this->session->set_flashdata('message', 'Mail Send Successfully');
                        redirect('united/sell');
                    } else {
                        $this->session->set_flashdata('message', 'There is some problem in sending the email');
                        redirect('united/sell');
                    }
                } else {
                    $this->session->set_flashdata('message', 'There is some problem in sending the email');
                    redirect('united/sell');
                }
            } else {
                $this->session->set_flashdata('message', 'Invalid email Address');
                redirect('united/sell');
            }
        }

    }

    public function imageUpload()
    {
        /*header('Access-Control-Allow-Origin: *');
        $target_path = "uploads/image_picker/";
        $target_path = $target_path . basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            $data['success'] = "Upload and move success";
            $data['path'] = $target_path;
        } else {
            $data['path'] = $target_path;
            $data['error'] = "There was an error uploading the file, please try again!";
        }*/

        header('Access-Control-Allow-Origin: *');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            $data = array();
            $config = array(
                'upload_path' => './uploads/' . $folderPath,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $data['error'] = $error;
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];

                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'qty' => 0,
                        'name' => '',
                        'price' => 0,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $insert = $this->united->create($array_data, $folderPath);
                    if ($insert) {
                        $data['success'] = "Image uploaded successfully";
                    } else {
                        $data['error'] = "Image not uploaded";
                    }
                } else {
                    $data['error'] = "Image not uploaded yet..";
                }
            }
        } else {
            $data['error'] = 'folder path not defined';
        }
        echo json_encode($data);
    }

    public function imageList()
    {
        header('Access-Control-Allow-Origin: *');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            $data['success'] = $this->united->allrecord($folderPath);
        } else {
            $data['error'] = 'image path not defined';
        }
        echo json_encode($data);
    }

    public function imageEditList()
    {
        header('Access-Control-Allow-Origin: *');
        $name = $this->input->post('name');
        $price = $this->input->post('price');
        $qty = $this->input->post('qty');
        $id = $this->input->post('id');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            if ($name && $price && $id) {
                $updatedData = array(
                    'name' => isset($name) ? $name : '',
                    'price' => isset($price) ? $price : '',
                    'qty' => isset($qty) ? $qty : '',
                    'created_at' => date("m/d/y h:i:s")
                );
                $this->db->where('id', $id);
                $this->db->update('united_' . $folderPath, $updatedData);
                $data['success'] = 'Record updated successfully';

            } else {
                $data['error'] = 'values required';
            }
        } else {
            $data['error'] = 'image path not defined';
        }
        echo json_encode($data);
    }

    public function imageDeleteList()
    {
        header('Access-Control-Allow-Origin: *');
        $id = $this->input->post('id');
        $image_name = $this->input->post('image_name');
        $folderPath = $this->input->post('image_path');
        if ($folderPath) {
            if ($id) {
                unlink("uploads/" . $folderPath . "/" . $image_name);
                $this->db->where('id', $id);
                $this->db->delete('united_' . $folderPath);
                $data['success'] = 'Record deleted successfully';
            } else {
                $data['error'] = 'values required';
            }
        } else {
            $data['error'] = 'image path not defined';
        }
        echo json_encode($data);
    }

}