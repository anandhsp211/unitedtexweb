<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Users_model', 'users');
        $this->load->model('UnitedEntry_model', 'united');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function index()
    {
        $this->happylist();
    }

    public function login_data()
    {
        $data = array(
            'username' => trim($this->input->post('username')),
            'password' => md5(trim($this->input->post('password')))
        );
        $get_data = $this->users->checkusername($data);
        if (isset($get_data) && is_array($get_data) && !empty($get_data)) {
            $user_session = array(
                'id' => $get_data[0]['id'],
                'firstname' => $get_data[0]['firstname'],
                'lastname' => $get_data[0]['lastname'],
                'username' => $get_data[0]['username'],
                'email' => $get_data[0]['email'],
                'role_id' => $get_data[0]['role_id'],
                'sex' => $get_data[0]['sex'],
                'address' => $get_data[0]['address']
            );
            $this->session->set_userdata($user_session);
            $this->session->set_flashdata('message', 'You have been successfully logged ..');
            redirect('home/happylist');
        } else {
            $this->session->set_flashdata('message', 'Invalid Login details');
            redirect('login');
        }
    }

    public function home()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/recent');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function view()
    {
        $this->home();
    }

    public function productadd()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/productadd');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function happy()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/happy');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function happylist()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $data['data'] = $this->united->allrecord('happy');
            $this->load->view('template/happylist', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function happyedit($id)
    {

        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            if ($id) {
                $data['data'] = $this->united->edit_data('happy', $id);
                $this->load->view('template/happyedit', $data, FALSE);
            } else {
                $this->session->set_flashdata('message', 'You have been login First..');
                redirect('login');
            }
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function happyeditupload()
    {
        $name = $this->input->post('name');
        $id = $this->input->post('id');
        $position = $this->input->post('position');
        $desc = $this->input->post('descr');
        $image_old_name = $this->input->post('image_old_name');
        $dbName = 'happy';
        if ($id && $image_old_name) {
            unlink("uploads/" . $dbName . "/" . $image_old_name);
        }
        if ($name && $position) {
            $config = array(
                'upload_path' => './uploads/happy',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('home/view');
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'name' => $name,
                        'position' => $position,
                        'descr' => $desc,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $this->db->where('id', $id);
                    $update = $this->db->update('united_' . $dbName, $array_data);
                    if ($update) {
                        $this->session->set_flashdata('message', 'Happy customer data updated successfully..');
                        redirect('home/happylist');
                    } else {
                        $this->session->set_flashdata('message', 'Happy customer data not uploaded..');
                        redirect('home');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Happy customer data not uploaded yet..');
                    redirect('home');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/view');
        }

    }

    public function happydelete($id)
    {
        $dbName = 'happy';
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('united_' . $dbName);
            $this->session->set_flashdata('message', 'Happy customer data deleted Successfully..');
            redirect('home/happylist');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function happyUpload()
    {
        $name = $this->input->post('name');
        $position = $this->input->post('position');
        $desc = $this->input->post('descr');
        $dbName = 'happy';

        if ($name && $position) {
            $config = array(
                'upload_path' => './uploads/happy',
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('home/view');
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'name' => $name,
                        'position' => $position,
                        'descr' => $desc,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $insert = $this->united->create($array_data, $dbName);
                    if ($insert) {
                        $this->session->set_flashdata('message', 'Happy customer data uploaded successfully..');
                        redirect('home/happylist');
                    } else {
                        $this->session->set_flashdata('message', 'Happy customer data not uploaded..');
                        redirect('home');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Happy customer data not uploaded yet..');
                    redirect('home');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/view');
        }
    }

    public function recent()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $this->load->view('template/recent');
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function recentlist()
    {
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $data['data'] = $this->united->allrecord('recent');
            $this->load->view('template/recentlist', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function recentedit($id)
    {

        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            if ($id) {
                $data['data'] = $this->united->edit_data('recent', $id);
                $this->load->view('template/recentedit', $data, FALSE);
            } else {
                $this->session->set_flashdata('message', 'You have been login First..');
                redirect('login');
            }
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function recenteditUpload()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $image_old_name = $this->input->post('image_old_name');
        $dbName = 'recent';
        if ($id && $image_old_name) {
            unlink("uploads/" . $dbName . "/" . $image_old_name);
        }
        if ($name) {
            $config = array(
                'upload_path' => './uploads/' . $dbName,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('home/view');
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'name' => $name,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $this->db->where('id', $id);
                    $update = $this->db->update('united_' . $dbName, $array_data);
                    if ($update) {
                        $this->session->set_flashdata('message', 'Recent work data updated successfully..');
                        redirect('home/recentlist');
                    } else {
                        $this->session->set_flashdata('message', 'Recent work data not updated..');
                        redirect('home');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Recent work data not updated yet..');
                    redirect('home');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/view');
        }

    }

    public function recentdelete($id)
    {
        $dbName = 'recent';
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->db->where('id', $id);
            $this->db->delete('united_' . $dbName);
            $this->session->set_flashdata('message', 'Recent work data deleted Successfully..');
            redirect('home/recentlist');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }

    }

    public function recentUpload()
    {
        $name = $this->input->post('name');
        $dbName = 'recent';

        if ($name && $dbName) {
            $config = array(
                'upload_path' => './uploads/' . $dbName,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                redirect('home/view');
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
                if ($image_name) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'name' => $name,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $insert = $this->united->create($array_data, $dbName);
                    if ($insert) {
                        $this->session->set_flashdata('message', 'Recent work Data uploaded successfully..');
                        redirect('home/recentlist');
                    } else {
                        $this->session->set_flashdata('message', 'Recent work Data not uploaded..');
                        redirect('home');
                    }
                } else {
                    $this->session->set_flashdata('message', 'User Data not uploaded yet..');
                    redirect('home');
                }
            }
        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/view');
        }
    }

    public function productUpload()
    {
        $folderPath = trim($this->input->post('image_path'));
        $name = trim($this->input->post('name'));
        $price = trim($this->input->post('price'));
        $qty = trim($this->input->post('qty'));
        $desc = trim($this->input->post('descr'));
        $featured_list = trim($this->input->post('featured_list'));
        if ($folderPath) {
            $config = array(
                'upload_path' => './uploads/' . $folderPath,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
            }
            if ($name && $qty) {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
                if(!$image_name){
                    $image_name = '';
                }
                if ($name && $qty) {
                    $array_data = array(
                        'image_name' => $image_name,
                        'qty' => $qty,
                        'name' => $name,
                        'price' => $price,
                        'value' => $folderPath,
                        'featured_list' => $featured_list,
                        'descr' => $desc,
                        'created_at' => date("m/d/y h:i:s")
                    );
                    $insert = $this->united->create($array_data, $folderPath);
                    if ($insert) {
                        $this->session->set_flashdata('message', $folderPath . ' data updated successfully..');
                        redirect('home/productlist?data=' . $folderPath);
                    } else {
                        $this->session->set_flashdata('message', 'Data not uploaded..');
                        redirect('home/productlist?data=' . $folderPath);
                    }
                } else {
                    $this->session->set_flashdata('message', 'Data not uploaded yet..');
                    redirect('home/productlist?data=' . $folderPath);
                }
            }
        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/productlist?data=' . $folderPath);
        }
    }

    public function productlist()
    {
        $divSel = '';
        $get_val = $this->input->get();
        if (isset($get_val) && $get_val) {
            $divSel = $get_val['data'];
        } else {
            $divSel = 'looms';
        }
        $usr = $this->session->userdata('id');
        if (isset($usr) && trim($usr != '')) {
            $this->load->view('template/header');
            $data['looms'] = $this->united->allrecord('looms');
            $data['spares'] = $this->united->allrecord('spares');
            $data['fabric'] = $this->united->allrecord('fabric');
            $data['high'] = $divSel;
            $this->load->view('template/productlist', $data, FALSE);
            $this->load->view('template/footer');
        } else {
            $this->session->set_flashdata('message', 'You have been login First..');
            redirect('login');
        }
    }

    public function productedit($id)
    {
        $get_val = $this->input->get();
        $usr = $this->session->userdata('id');
        if (isset($get_val) && $get_val) {
            $folder_db_sel = '';
            if (isset($get_val['looms']) && $get_val['looms'] == true) {
                $folder_db_sel = 'looms';
            } else if (isset($get_val['spares']) && $get_val['spares'] == true) {
                $folder_db_sel = 'spares';
            } else if (isset($get_val['fabric']) && $get_val['fabric'] == true) {
                $folder_db_sel = 'fabric';
            }

            if (isset($usr) && trim($usr != '')) {
                $this->load->view('template/header');
                if ($id) {
                    $data['data'] = $this->united->edit_data($folder_db_sel, $id);
                    $this->load->view('template/productedit', $data, FALSE);
                } else {
                    $this->session->set_flashdata('message', 'You have been login First..');
                    redirect('login');
                }
                $this->load->view('template/footer');
            } else {
                $this->session->set_flashdata('message', 'You have been login First..');
                redirect('login');
            }
        }
    }

    public function producteditupload()
    {

        $id = trim($this->input->post('id'));
        $name = trim($this->input->post('name'));
        $image_path = trim($this->input->post('image_path'));
        $price = trim($this->input->post('price'));
        $qty = trim($this->input->post('qty'));
        $descr = trim($this->input->post('descr'));
        $image_old_name = trim($this->input->post('image_old_name'));
        $featured_list = trim($this->input->post('featured_list'));

        $dbName = $image_path;

        if ($name && $id) {
            $config = array(
                'upload_path' => './uploads/' . $dbName,
                'allowed_types' => 'gif|jpg|png',
                'max_size' => '1024000',
                'max_width' => '102400000',
                'max_height' => '76800000',
                'encrypt_name' => true,
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $this->session->set_flashdata('message', $this->upload->display_errors());
                $image_name = $image_old_name;
            } else {
                $upload_data = $this->upload->data();
                $image_name = $upload_data['file_name'];
            }

            if ($name && $price) {
                $array_data = array(
                    'image_name' => $image_name,
                    'qty' => $qty,
                    'name' => $name,
                    'price' => $price,
                    'featured_list' => $featured_list,
                    'value' => $image_path,
                    'descr' => $descr,
                    'created_at' => date("m/d/y h:i:s")
                );
                $this->db->where('id', $id);
                $update = $this->db->update('united_' . $dbName, $array_data);
                if ($update) {
                    $this->session->set_flashdata('message', $image_path . ' data updated successfully..');
                    redirect('home/productlist?data=' . $image_path);
                } else {
                    $this->session->set_flashdata('message', $image_path . ' data not uploaded..');
                    redirect('home/productlist?data=' . $image_path);
                }
            } else {
                $this->session->set_flashdata('message', $image_path . ' data not uploaded yet..');
                redirect('home/productlist?data=' . $image_path);
            }

        } else {
            $this->session->set_flashdata('message', 'folder path not defined..');
            redirect('home/productlist?data=' . $image_path);
        }

    }

    public function productdelete($id)
    {
        $get_val = $this->input->get();
        $usr = $this->session->userdata('id');
        if (isset($get_val) && $get_val) {
            $folder_db_sel = '';
            if (isset($get_val['looms']) && $get_val['looms'] == true) {
                $folder_db_sel = 'looms';
            } else if (isset($get_val['spares']) && $get_val['spares'] == true) {
                $folder_db_sel = 'spares';
            } else if (isset($get_val['fabric']) && $get_val['fabric'] == true) {
                $folder_db_sel = 'fabric';
            }
            if (isset($usr) && trim($usr != '')) {
                if (isset($usr) && trim($usr != '')) {
                    $this->db->where('id', $id);
                    $this->db->delete('united_' . $folder_db_sel);
                    $this->session->set_flashdata('message', $folder_db_sel . ' data deleted successfully..');
                    redirect('home/productlist?data=' . $folder_db_sel);
                } else {
                    $this->session->set_flashdata('message', 'You have been login First..');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('message', 'You have been login First..');
                redirect('login');
            }
        }

    }

}