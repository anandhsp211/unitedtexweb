<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Employees_model', 'employee');
        $this->load->helper('form');
        $this->load->library('session');
    }

    public function create()
    {
        header('Access-Control-Allow-Origin: *');
        $target_path = "uploads/image_picker/";
        $target_path = $target_path . basename($_FILES['file']['name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            $data['success'] = "Upload and move success";
            $data['path'] = $target_path;
        } else {
            $data['path'] = $target_path;
            $data['error'] = "There was an error uploading the file, please try again!";
        }
        echo json_encode($data);
    }

    public function push()
    {
        $data = array('title'=>'Title of the notification', 'body'=>'hello', 'sound'=>'default');
        $target = '/topics/notetoall';
        $url = 'https://fcm.googleapis.com/fcm/send';
        $server_key = 'AIzaSyAt0sB7_MWyLbBhYSwLIKxkZE8Wi4sJhsU';
        $fields = array();
        $fields['notification'] = $data; // <-- this is what I changed from $fields['body']
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        }else{
            $fields['to'] = $target;
        }
    }
}