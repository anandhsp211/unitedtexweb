jQuery(document).ready(function ($) {
    "use strict";
    if (ScrollDiv) {
        $('html, body').animate({
            scrollTop: $(ScrollDiv).offset().top
        }, 'slow');
    }

    $(document).on('click', 'body *', function () {
        if (ScrollDiv) {
            ScrollDiv = '';
        }
    });

    $(function () {
        $("#tabs").tabs();
    });
    // Page loading animation

    $("#preloader").animate({
        'opacity': '0'
    }, 600, function () {
        setTimeout(function () {
            $("#preloader").css("visibility", "hidden").fadeOut();
        }, 300);
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        var box = $('.header-text').height();
        var header = $('header').height();

        if (scroll >=  header) {
            $("header").addClass("background-header");
        } else {
            $("header").removeClass("background-header");
        }
    });
    if ($('.owl-clients').length) {
        $('.owl-clients').owlCarousel({
            loop: true,
            nav: false,
            dots: true,
            items: 3,
            margin: 30,
            autoplay: true,
            smartSpeed: 700,
            autoplayTimeout: 6000,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                460: {
                    items: 1,
                    margin: 0
                },
                576: {
                    items: 3,
                    margin: 20
                },
                992: {
                    items: 4,
                    margin: 30
                }
            }
        });
    }

    if ($('.owl-testimonials').length) {
        $('.owl-testimonials').owlCarousel({
            loop: true,
            nav: true,
            dots: true,
            items: 1,
            margin: 30,
            autoplay: false,
            smartSpeed: 700,
            autoplayTimeout: 6000,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                460: {
                    items: 1,
                    margin: 0
                },
                576: {
                    items: 2,
                    margin: 20
                },
                992: {
                    items: 2,
                    margin: 30
                }
            }
        });
    }

    if ($('.owl-banner').length) {
        $('.owl-banner').owlCarousel({
            loop: true,
            dots: true,
            items: 1,
            margin: 0,
            autoplay: false,
            smartSpeed: 700,
            autoplayTimeout: 6000,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                    margin: 0
                },
                460: {
                    items: 1,
                    margin: 0
                },
                576: {
                    items: 1,
                    margin: 20
                },
                992: {
                    items: 1,
                    margin: 30
                }
            }
        });
    }

    $(".Modern-Slider").slick({
        autoplay: true,
        autoplaySpeed: 10000,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: false,
        dots: true,
        pauseOnDotsHover: true,
        cssEase: 'linear',
        // fade:true,
        draggable: false,
        prevArrow: '<button class="PrevArrow"></button>',
        nextArrow: '<button class="NextArrow"></button>',
    });

    $('.filters ul li').click(function () {
        $('.filters ul li').removeClass('active');
        $(this).addClass('active');

        var data = $(this).attr('data-filter');
        $grid.isotope({
            filter: data
        })
    });

    $(function() {
        $(".grid").isotope({filter: '.looms'});
    });

    var $grid = $(".grid").isotope({
        itemSelector: ".all",
        percentPosition: true,
        filter: '.transition',
        sortBy: 'name',
        layoutMode: 'fitRows',
        masonry: {
            columnWidth: ".all"
        }
    });

    $('.accordion > li:eq(0) a').addClass('active').next().slideDown();

    $('.accordion a').click(function (j) {
        var dropDown = $(this).closest('li').find('.content');

        $(this).closest('.accordion').find('.content').not(dropDown).slideUp();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).closest('.accordion').find('a.active').removeClass('active');
            $(this).addClass('active');
        }
        dropDown.stop(false, true).slideToggle();
        j.preventDefault();
    });

    $('.lazy').lazy();

    $("form[name='registration']").validate({
        rules: {
            name: "required",
            subject: "required",
            message: "required",
            email: {
                required: true,
                email: true
            },
            phoneNumber: {
                minlength: 10,
                maxlength: 10
            }
        },
        // Specify validation error messages
        messages: {
            name: "Please enter your name",
            subject: "Please enter your subject",
            message: "Please enter your message",
            email: "Please enter a valid email address",
            phoneNumber: "Please enter a valid mobile number"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });


    $("form[name='sell']").validate({
        rules: {
            category: "required",
            country: "required",
            name: "required",
            subject: "required",
            message: "required",
            email: {
                required: true,
                email: true
            },
            phoneNumber: {
                minlength: 10,
                maxlength: 10
            }
        },
        // Specify validation error messages
        messages: {
            category: "Please select category",
            country: "Please enter your country",
            name: "Please enter your name",
            subject: "Please enter your subject",
            message: "Please enter your message",
            email: "Please enter a valid email address",
            phoneNumber: "Please enter a valid mobile number"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
            form.submit();
        }
    });

    $('.toast').fadeIn(400).delay(3000).fadeOut(400);
    $('.toast_one').fadeIn(800).delay(6000).fadeOut(800);

    window.onscroll = function () {
        myFunction();
    };

    var has_class = $('li').hasClass('spares_active');
    if(has_class){
        $(".filters ul li.spares_active").addClass('active');
    }
    var navbar = document.getElementById("navbar");
    var header = $('header').height();
    var sticky = navbar.offsetTop;


    function myFunction() {
        if (window.pageYOffset >= header) {
            navbar.classList.add("sticky")
        } else {
            navbar.classList.remove("sticky");
        }
    }

});
